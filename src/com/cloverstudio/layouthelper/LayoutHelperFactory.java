package com.cloverstudio.layouthelper;


public class LayoutHelperFactory {

    public static LayoutHelper createLayoutHelper(float expectedWidth, float expectedHeight) {
        return new LayoutHelper(expectedWidth, expectedHeight);
    }

    public static LayoutHelper createLandscapeLayoutHelper(Resolution res) {
        return new LayoutHelper(res.getValueX(), res.getValueY());
    }

    public static LayoutHelper createPortraitLayoutHelper(Resolution res) {
        return new LayoutHelper(res.getValueY(), res.getValueX());
    }

}
