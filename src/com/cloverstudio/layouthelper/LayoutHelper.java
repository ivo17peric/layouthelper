package com.cloverstudio.layouthelper;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LayoutHelper {

    /* iPhone5 resolution in portrait */
    public static final float HEIGHT_PORTRAIT = 1280f;
    public static final float WIDTH_PORTRAIT = 720f;
    /* iPhone 5 resolution in landscape */
    public static final float WIDTH_LANDSCAPE = 1280f;
    public static final float HEIGHT_LANDSCAPE = 720f;

    private float lengthY;
    private float lengthX;

    protected LayoutHelper(float x, float y) {
        this.lengthX = x;
        this.lengthY = y;
    }

	public int getDisplayHeight(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.heightPixels;
	}

	public int getDisplayWidth(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		return dm.widthPixels;
	}

	public float getK(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

		float width = dm.widthPixels;
		float height = dm.heightPixels;

		float aspect = width / height;

		float k;
		if (aspect >= (lengthX/lengthY)) {
			k = height / lengthY;
		} else {
			k = width / lengthX;
		}
		return k;
	}

	public float getKForHeight(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

		float height = dm.heightPixels;

		float k;
		k = height / lengthY;
		return k;
	}

	public void scaleToFit(View parent, Activity activity) {
        calculateRecursive(parent, getK(activity));
	}

	public int getScaledWidth(Activity activity) {
		return (int) (lengthX * getK(activity));
	}

	public int getScaledHeight(Activity activity) {
		return (int) (lengthY * getK(activity));
	}

	public int getScaledItemHeight(Activity activity, int originalHeight) {

		int itemHeight = (int) (originalHeight / lengthY * getScaledWidth(activity));
		return itemHeight;
	}

	private void calculateRecursive(View parent, float k) {
		if ((parent instanceof FrameLayout) || (parent instanceof LinearLayout)
                || (parent instanceof RelativeLayout) || (parent instanceof GridLayout)) {
			ViewGroup group = (ViewGroup) parent;
			for (int i = 0; i < group.getChildCount(); i++) {
				View view = group.getChildAt(i);

				if (parent instanceof FrameLayout) {
					FrameLayout.LayoutParams source = (FrameLayout.LayoutParams) (view.getLayoutParams());
					FrameLayout.LayoutParams dest = new FrameLayout.LayoutParams(view.getLayoutParams());

					dest.height = (source.height > 0) ? (int) (source.height * k) : source.height;
					dest.width = (source.width > 0) ? (int) (source.width * k) : source.width;

					dest.leftMargin = (int) (source.leftMargin * k);
					dest.rightMargin = (int) (source.rightMargin * k);
					dest.topMargin = (int) (source.topMargin * k);
					dest.bottomMargin = (int) (source.bottomMargin * k);

					dest.gravity = source.gravity;
                    if (dest.gravity < 0) {
                        dest.gravity = Gravity.TOP | Gravity.LEFT;
                    }

					view.setLayoutParams(dest);

				} else if (parent instanceof LinearLayout) {
					LinearLayout.LayoutParams source = (LinearLayout.LayoutParams) (view.getLayoutParams());
					LinearLayout.LayoutParams dest = new LinearLayout.LayoutParams(view.getLayoutParams());

					dest.height = (source.height > 0) ? (int) (source.height * k) : source.height;
					dest.width = (source.width > 0) ? (int) (source.width * k) : source.width;

					dest.leftMargin = (int) (source.leftMargin * k);
					dest.rightMargin = (int) (source.rightMargin * k);
					dest.topMargin = (int) (source.topMargin * k);
					dest.bottomMargin = (int) (source.bottomMargin * k);

					dest.gravity = source.gravity;
					dest.weight = source.weight;

					view.setLayoutParams(dest);
				} else if (parent instanceof RelativeLayout) {
					RelativeLayout.LayoutParams source = (RelativeLayout.LayoutParams) (view.getLayoutParams());
					RelativeLayout.LayoutParams dest = new RelativeLayout.LayoutParams(view.getLayoutParams());

					dest.height = (source.height > 0) ? (int) (source.height * k) : source.height;
					dest.width = (source.width > 0) ? (int) (source.width * k) : source.width;

					dest.leftMargin = (int) (source.leftMargin * k);
					dest.rightMargin = (int) (source.rightMargin * k);
					dest.topMargin = (int) (source.topMargin * k);
					dest.bottomMargin = (int) (source.bottomMargin * k);

					dest.alignWithParent = source.alignWithParent;

					int rule[] = source.getRules();
					for (int j = 0; j < rule.length; j++) {
						dest.addRule(j, rule[j]);
					}
					view.setLayoutParams(dest);
				} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && parent instanceof GridLayout) {
                    editGridLayoutParameters(view, k);
                }

                int paddingLeft = (int) (view.getPaddingLeft() * k);
                int paddingTop = (int) (view.getPaddingTop() * k);
                int paddingRight = (int) (view.getPaddingRight() * k);
                int paddingBottom = (int) (view.getPaddingBottom() * k);

                view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);

				if ((view instanceof FrameLayout) || (view instanceof LinearLayout) || (view instanceof TextView)
						|| (view instanceof RelativeLayout)) {
					calculateRecursive(view, k);
				}
			}
		} else if (parent instanceof TextView) {
			TextView text = (TextView) parent;
			text.setTextSize(TypedValue.COMPLEX_UNIT_PX, text.getTextSize() * k);
		}
	}

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void editGridLayoutParameters(View view, float k) {
        GridLayout.LayoutParams source = (GridLayout.LayoutParams) (view.getLayoutParams());
        GridLayout.LayoutParams dest = new GridLayout.LayoutParams(view.getLayoutParams());

        dest.height = (source.height > 0) ? (int) (source.height * k) : source.height;
        dest.width = (source.width > 0) ? (int) (source.width * k) : source.width;

        dest.leftMargin = (int) (source.leftMargin * k);
        dest.rightMargin = (int) (source.rightMargin * k);
        dest.topMargin = (int) (source.topMargin * k);
        dest.bottomMargin = (int) (source.bottomMargin * k);

        dest.columnSpec = source.columnSpec;
        dest.rowSpec = source.rowSpec;
    }

	public float getOldK(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

		float width = dm.widthPixels;
		float height = dm.heightPixels;

		float aspect = width / height;

		float k;
		if (aspect >= 1.5f) {
			k = height / 480f;
		} else {
			k = width / 720f;
		}

		return k;
	}
}
